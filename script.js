//dom elements

const container = document.querySelector('.container'); // screen + seats

const avaiableSeats = //seats
document.querySelectorAll('.row .seat:not(.occupied)'); // returns node list/array of seats

const seatCount = document.getElementById('numberOfSeats'); // count

const totalPrice = document.getElementById('total'); // total

const movieSelected = document.getElementById('movie');

let ticketPrice = parseInt(movieSelected.value);

function populateUI(){
    // get data from local storage

    //convert from string back to array
    const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));

    if(selectedSeats != null && selectedSeats.length > 0){ // if there are selected seats
        avaiableSeats.forEach((availableSeat, avaiableSeatIndex) => { // go through available seats
            if(selectedSeats.indexOf(avaiableSeatIndex) > -1){
                availableSeat.classList.add('selected');
            }
        });  
    }

    // retrieve movie index from storage
    const selectedMovieIndex = localStorage.getItem('movieNameIndex');

    //set movie name
    if(selectedMovieIndex != null){
        movieSelected.selectedIndex = selectedMovieIndex;
    }
}


//save selected movie index and price
const setMoviedData = (movieNameIndex, moviePrice) => {
    localStorage.setItem('movieNameIndex', movieNameIndex);
    localStorage.setItem('moviePrice', moviePrice);
};

const updateSelectedSeatCountAndPrice = () => {
    //retrieve selected seat count
    const selectedSeats = document.querySelectorAll('.row .seat.selected'); // array

    //Retrive Index of Selected Seat
    //go through 'selected seats'
    //return back 'index' of 'available seat' - IF avaiable
    //If the 'seat selected' is not in the 'avaiable seats' -> no index will be returned
    const selectedSeatsIndex = [...selectedSeats].map(function(seat){
        return [...avaiableSeats].indexOf(seat);
    });    

    console.log("selected seats in updateSelectedSeats()");
    console.log(selectedSeatsIndex);
    
    
    //save seats to local storage
    //Json.stringify - convert from array to string
    localStorage.setItem('selectedSeats', JSON.stringify(selectedSeatsIndex));
    

    //map through array - return new array of indexes via spread operator

    const selectedSeatCount = selectedSeats.length;

    //set seat count
    seatCount.innerText = selectedSeatCount;

    //update total ticket price
    totalPrice.innerText = selectedSeatCount * ticketPrice;
    
};

//movie changed - seat selected
movieSelected.addEventListener('change', e => {
    ticketPrice = parseInt(e.target.value);

    //retrieve movie index and price 
    const movieNameIndex = e.target.selectedIndex;
    const moviePrice = e.target.value;
    setMoviedData(movieNameIndex, moviePrice);

    updateSelectedSeatCountAndPrice();
});

//seat selected
container.addEventListener('click', (event) => {

    //toggle selected seat
    if( event.target.classList.contains('seat') && !event.target.classList.contains('occupied')){
        event.target.classList.toggle('selected');

        updateSelectedSeatCountAndPrice();
    }
});

populateUI();
updateSelectedSeatCountAndPrice();